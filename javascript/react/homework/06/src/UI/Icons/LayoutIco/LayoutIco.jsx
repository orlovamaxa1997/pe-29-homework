import PropTypes from 'prop-types';

export function LayoutIco({ width, fill }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 122.43 122.88"
      width={width}
    >
      <path
        d="M3.27,0H119.61a3.26,3.26,0,0,1,3.27,3.27V115.69a3.26,3.26,0,0,1-3.27,3.27H3.27A3.26,3.26,0,0,1,0,115.69V3.27A3.26,3.26,0,0,1,3.27,0ZM116.34,81.49H6.54v30.93h109.8V81.49Zm0-37.48H6.54V75h109.8V44ZM6.54,6.54V37.47h109.8V6.54Z"
        fill={fill}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

LayoutIco.propTypes = {
  width: PropTypes.number,
  fill: PropTypes.string,
};
