import React from 'react';
import styles from './Shop.module.scss';
import { Product } from '../../components/Product/Product';
// import { useOutletContext } from 'react-router-dom';
// import { useOutletContext } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { Loader } from '../../UI/Loader/Loader';

const getProducts = (state) => state.products;

export const Shop = () => {
  // get status upload products
  const { error, status, items } = useSelector(getProducts);

  return (
    <div className={styles.Shop}>
      {status === 'pending' && <Loader />}
      {error && <h2>{error}</h2>}

      {!error && status === 'fulfilled' && (
        <ul className={styles.ShopList}>
          {items.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
