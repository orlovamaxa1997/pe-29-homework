import React from 'react';
import styles from './Basket.module.scss';
import { Product } from '../../components/Product/Product';

import { useSelector } from 'react-redux';

const getProducts = (state) => state.products;

export const Basket = () => {
  // get products in basket
  const { items, productsInBasket } = useSelector(getProducts);

  // set product in basket
  const productsBasket = items.filter((product) =>
    productsInBasket.find((item) => item.id === product.id)
  );

  // check flag if basket is empty
  const emptyBasket = !productsBasket.length;

  return (
    <div className={styles.Basket}>
      {emptyBasket && <h1>Товаров нет</h1>}
      {!emptyBasket && (
        <ul className={styles.BasketList}>
          {productsBasket.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
