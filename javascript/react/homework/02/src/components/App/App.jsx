import React, { Component } from 'react';
import styles from './App.module.scss';

import { Service } from '../../API/Services';
import { Goods } from '../Goods/Goods';
import { Header } from '../Header/Header';

export class App extends Component {
  constructor() {
    super();

    this.state = {
      error: null,
      isLoaded: false,
      headphones: [],
      goodsStarId: [],
      goodsBasketId: [],
    };
  }

  componentDidMount() {
    Service.getAll()
      .then((response) => response.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            headphones: result.headphones,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );

    this.getStorageValue('goodsStarId');
    this.getStorageValue('goodsBasketId');
  }

  getStorageValue = (name) => {
    const storageId = localStorage.getItem(name);
    if (storageId) {
      const newStateValue = storageId
        .split(',')
        .map((storageId) => Number(storageId));
      this.setState({
        [name]: newStateValue,
      });
    }
  };

  addStorageValue = (name, id) => {
    const newStateValue = this.state[name];
    newStateValue.push(id);
    this.setState({
      [name]: newStateValue,
    });
    localStorage.setItem(name, newStateValue);
  };

  removeStorageValue = (name, id) => {
    let newStateValue = [];
    const oldStateValue = this.state[name];
    if (oldStateValue.length) {
      newStateValue = oldStateValue.filter((storageId) => storageId !== id);
      this.setState({
        [name]: newStateValue,
      });
      localStorage.setItem(name, newStateValue);
    }
  };

  render() {
    const { error, isLoaded, headphones } = this.state;
    if (error) {
      return <div>Помилка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Завантаження...</div>;
    } else {
      return (
        <div className={styles.App}>
          <Header
            goodsStarCount={this.state.goodsStarId.length}
            goodsBasketCount={this.state.goodsBasketId.length}
          />
          <Goods
            headphones={headphones}
            addStorageValue={this.addStorageValue}
            removeStorageValue={this.removeStorageValue}
          />
        </div>
      );
    }
  }
}
