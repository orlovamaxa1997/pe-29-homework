import React, { Component } from 'react';
import styles from './Goods.module.scss';
import PropTypes from 'prop-types';

import { Good } from '../Good/Good';

export class Goods extends Component {
  constructor(props) {
    super(props);
    this.headphones = props.headphones;
  }
  render() {
    return (
      <div className={styles.GoodsContainer}>
        <ul className={styles.Goods}>
          {this.headphones.map((item) => (
            <Good
              key={item.id}
              item={item}
              addStorageValue={this.props.addStorageValue}
              removeStorageValue={this.props.removeStorageValue}
            ></Good>
          ))}
        </ul>
      </div>
    );
  }
}

Goods.propTypes = {
  headphones: PropTypes.array.isRequired,

  addStorageValue: PropTypes.func.isRequired,
  removeStorageValue: PropTypes.func.isRequired,
};
