import styled from 'styled-components';

export const MyButton = styled.button`
  cursor: pointer;
  border: none;
  padding: 10px 15px;
  color: #fff;
  text-transform: uppercase;
  border-radius: 5px;
  transition: all 0.3s;
  background-color: ${({ className }) => {
    switch (className) {
        case 'red':
            return '#b3382c';
            break;
        case 'green':
            return '#34ab40';
            break;
        case 'blue':
            return '#5661e1';
            break;
        default:
            return '#e74c3c';
    }
}};

  &:hover {
    filter: contrast(200%);
  }
`;

