import React from 'react';
import { useOutletContext } from 'react-router-dom';
import styles from './Shop.module.scss';
import { Product } from '../../components/Product/Product';
import PropTypes from 'prop-types';

export const Shop = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInBasket,
    handleBasketClick,
    setModal,
    setModalData,
  } = useOutletContext();

  return (
    <div className={styles.Shop}>
      <ul className={styles.ShopList}>
        {products.map((product) => (
          <Product
            key={product.id}
            product={product}
            favorites={favorites}
            handleFavoritesClick={handleFavoritesClick}
            productsInBasket={productsInBasket}
            handleBasketClick={handleBasketClick}
            setModal={setModal}
            setModalData={setModalData}
          ></Product>
        ))}
      </ul>
    </div>
  );
};

Shop.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInBasket: PropTypes.array.isRequired,
  handleBasketClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};
