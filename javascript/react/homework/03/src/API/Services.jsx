export class Service {
  static async getAll() {
    const response = await fetch('/db/db.json');
    const data = await response.json();
    return data;
  }
}
