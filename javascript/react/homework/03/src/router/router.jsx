import { createBrowserRouter } from 'react-router-dom';

import { App } from '../components/App/App';
import { Notfound } from '../pages/Notfound/Notfound';
import { Shop } from '../pages/Shop/Shop';
import { Basket } from '../pages/Basket/Basket';
import { Favorites } from '../pages/Favorites/Favorites';

export const router = createBrowserRouter([
  {
    element: <App />,
    path: '/',
    errorElement: <Notfound />,
    children: [
      {
        element: <Shop />,
        index: true,
      },
      {
        element: <Basket />,
        path: 'basket',
      },
      {
        element: <Favorites />,
        path: 'favorites',
      },
    ],
  },
]);
