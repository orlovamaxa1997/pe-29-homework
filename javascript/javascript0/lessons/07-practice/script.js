// function createSettingManage() {
//
// let setting = {};
//
// function setSetting() {
//     let key = prompt("Введіть назву налаштування профілю");
//     let value = prompt("Введіть значення налаштування");
//     setting[key] = value;
//     alert(`Налаштування ${key} зі значення ${value} було додано`);
// }
//
// function getSetting() {
//     let key = prompt("Введіть назву налаштування, яке ви хочете отримати");
//     let value = setting[key];
//     if (value){
//         alert(`Значення налаштування ${value}`)
//     } else {
//         alert(`Налаштування з назвою ${key} не знайдено`)
//     }
// }
//
// function updateSetting() {
//     let key = prompt("Введіть назву налаштування яке хочете оновити");
//     let value = setting[key];
//     let isReadyToUpdate = confirm(`Ви впевнені що хочете оновити значення налаштування ${key}?`);
//     if (isReadyToUpdate){
//         if (value){
//             let newValue = prompt(`Введіть нове значенн налаштування ${key}`);
//             setting[key] = newValue;
//             alert(`Налаштування ${key} оновлено до значення ${newValue}`);
//         } else {
//             alert(`Налаштування з назвою ${key} не знайдено`);
//         }
//     }else {
//         return;
//     }
// }
// function deleteSetting() {
//     let key = prompt("Введіть назву налаштування яке хочете видалити");
//     let value = setting[key];
//     let isReadyToUpdate = confirm(`Ви впевнені що хочете оновити значення налаштування ${key}?`);
//     if (isReadyDelete){
//         if (value) {
//             delete setting[key];
//             alert(`Значення налаштування ${key} видалено`);
//         } else {
//             alert(`Налаштування з назвою ${key} не знайдено`);
//         }
//     } else {
//         return;
//     }
// }
// }

function createOrderSystem() {
    let order = {
        totalPrice: 0,
        isDiscountApplied: false,
    }
    function addItem(price) {
        order.totalPrice += price;
    }
    function applyDiscount(discountFunction) {
        if (!order.isDiscountApplied){
            order.totalPrice = discountFunction(order.totalPrice);
            order.isDiscountApplied = true;
        }
    }
    function describeOrderPrice() {
        console.log(`Загальна вартість замовлення : ${order.totalPrice} Знижка застосована : ${order.isDiscountApplied ? "yes" : "no"}`);
    }
    return {addItem, applyDiscount, describeOrderPrice}
}

const orderSystem = createOrderSystem();
orderSystem.addItem(200);
orderSystem.addItem(150);
orderSystem.describeOrderPrice();

function tenPersonDiscount(total) {
    return total * 0.9;
}
orderSystem.applyDiscount(tenPersonDiscount);
orderSystem.describeOrderPrice()