// <!-- <h1>Title for a list</h1> -->
// <ul class="list">
//     <!-- <li>Item 1</li> -->
//     <li>Item 2</li>
// <li>Item 3</li>
//       <!-- <li>Item 4</li> -->
// </ul>
//     <!-- <p>Lorem ipsum dolor sit amet.</p> -->
//
// const taskInput = document.getElementById("taskInput");
// const addTaskBtn = document.getElementById("addTaskBtn");
// const taskList = document.querySelector("#taskList");
//
// console.dir(taskInput);
//
// function addTaskToTheTaskList() {
//     const taskName = taskInput.value.trim();
//     if (taskName !== "") {
//         const taskItem = document.createElement("div");
//         taskItem.classList.add("taskItem");
//
//         const p = document.createElement("p");
//         p.textContent = taskName;
//
//         const deleteButton = document.createElement("Button");
//         deleteButton.textContent = "❌";
//         deleteButton.classList.add("deleteButton")
//
//         taskItem.append(p);
//         taskItem.append(deleteButton);
//         taskList.append(taskItem);
//         taskInput.value = "";
//
//         deleteButton.addEventListener("click", function() {
//             taskItem.remove();
//         });
//     }
// }
//
// addTaskBtn.addEventListener("click", addTaskToTheTaskList);

const openModelButton = document.getElementById("openModalBtn");
const modalContainer = document.querySelector("#modalContainer");
const closeModelButton = document.getElementById("closeModalBtn");

openModelButton.addEventListener("click", () => {
    modalContainer.style.display = "block";
})


closeModelButton.addEventListener("click", () => {
    modalContainer.style.display = "none";
})