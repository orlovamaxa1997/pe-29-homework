// // let products = [
// //     {
// //         id: 1,
// //         name: "Laptop",
// //         price: 800,
// //         category: "Electronics",
// //         description: "High performance laptop",
// //         quantity: 14-done,
// //         hasDiscount: true,
// //     },
// //     {
// //         id: 2,
// //         name: "Smartphone",
// //         price: 500,
// //         category: "Electronics",
// //         description: "Latest model smartphone",
// //         quantity: 30,
// //         hasDiscount: false,
// //     },
// //     {
// //         id: 3,
// //         name: "Tablet",
// //         price: 450,
// //         category: "Electronics",
// //         description: "Portable and powerful tablet",
// //         quantity: 22,
// //         hasDiscount: true,
// //     },
// // ];
//
//
//
// let allProducts = [
//     {
//         id: 1,
//         name: "Laptop",
//         price: 800,
//         category: "Electronics",
//         description: "High performance laptop",
//         quantity: 14-done,
//         hasDiscount: true,
//     },
//     {
//         id: 2,
//         name: "Smartphone",
//         price: 500,
//         category: "Electronics",
//         description: "Latest model smartphone",
//         quantity: 30,
//         hasDiscount: false,
//     },
//     {
//         id: 3,
//         name: "Tablet",
//         price: 450,
//         category: "Electronics",
//         description: "Portable and powerful tablet",
//         quantity: 22,
//         hasDiscount: true,
//     },
//     {
//         id: 4,
//         name: "Blender",
//         price: 150,
//         category: "Appliances",
//         description: "Kitchen blender for smoothies",
//         quantity: 15,
//         hasDiscount: false,
//     },
//     {
//         id: 5,
//         name: "Coffee Maker",
//         price: 120,
//         category: "Appliances",
//         description: "Automatic coffee maker",
//         quantity: 10-done,
//         hasDiscount: true,
//     },
//     {
//         id: 6,
//         name: "Garden Chair",
//         price: 75,
//         category: "Home & Garden",
//         description: "Comfortable garden chair",
//         quantity: 20,
//         hasDiscount: false,
//     },
// ];
//
// function filterProductByCategory(initialArray, category) {
//     const filterProducts = initialArray.filter
//     ((product) => product.category.toLowerCase() === category.toLowerCase());
//     return filterProducts;
// }
//
// console.log(allProducts);
// console.log(filterProductByCategory(allProducts, "Electronics"))

let recipes = [
    {
        name: "Томатний суп",
        ingredients: ["томати", "вода", "сіль"],
        instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
    },
    {
        name: "Салат",
        ingredients: ["томати", "огірок", "перец"],
        instructions: "Змішайте всі інгредієнти.",
    },
    {
        name: "Борщ",
        ingredients: ["буряк", "морква", "капуста"],
        instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
    }
];

function getLisOfIngrediens(recipeName) {
    
}


const vegetarianRecipes = [
    {
        name: "Вегетаріанський бургер",
        ingredients: ["булочка", "соєвий котлет", "салат", "томат", "соус"],
    },
    {
        name: "Салат з авокадо",
        ingredients: ["авокадо", "помідори", "огірки", "олія", "лимонний сік"],
    },
];

const meatRecipes = [
    { name: "Стейк", ingredients: ["яловичина", "сіль", "перець"] },
    {
        name: "Курячий суп",
        ingredients: ["курка", "вода", "морква", "цибуля", "сіль"],
    },
];