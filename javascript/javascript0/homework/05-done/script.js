// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function partNum(num1, num2) {
    return num1 % num2;
}

console.log(partNum(23,10));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

//- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число

let num1 = parseInt(prompt("Введіть перше число"));
let num2 = parseInt(prompt("Введіть друге число"));


while (isNaN(parseInt(num1))) {
    num1 = parseInt(prompt("Введіть перше число"));
}
while (isNaN(parseInt(num2))) {
    num2 = parseInt(prompt("Введіть друге число"));
}

//- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').

let math = prompt("Введіть математичний знак +, -, *, /");

if (math === "+" || math === "-" || math === "*" || math === "/") {
   // console.log(math);
} else {
    alert('Такої операції не існує');
}

while (math !== "+" && math !== "-" && math !== "*" && math !== "/"){
    math = prompt("Введіть математичний знак +, -, *, /");
}


// - Створити функцію, в яку передати два значення та операцію.

function calculate (number1, mathOperation, number2) {
    if (mathOperation === "+"){
        return number1 + number2;
    } else if(mathOperation === "-") {
        return  number1 - number2;
    } else if(mathOperation === "*") {
        return number1 * number2;
    } else if(mathOperation === "/") {
       return  number1 / number2;
    }
}

// - Вивести у консоль результат виконання функції.

console.log(calculate(num1, math, num2));




