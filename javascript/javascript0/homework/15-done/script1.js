// Практичне завдання 1:
//
// -Створіть HTML-файл із кнопкою та елементом div.
//
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const buttonEl = document.getElementById("Button");

buttonEl.addEventListener("click", () => {
    const textContent = document.getElementById("textContent");
    setTimeout(() => {
        textContent.innerText = "Операція виконана успішно";
    }, 3000)
})