// Практичне завдання 2:
//
// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const countDown = document.getElementById("countdown");

function updateCount(count) {
    countDown.textContent = count;
    if (count === 0){
        countDown.textContent = "Зворотній відлік завершено";
    } else {
        setTimeout(() => {
            updateCount(count -1);
        }, 1000)
    }
}
updateCount(10)
