// 1. Попросіть користувача ввести свій вік за допомогою prompt.
//Якщо вік менше 12-done років, виведіть у вікні alert повідомлення про те,
//що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
//що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

let userAge = prompt("How is your age?");

if (userAge <= 12){
    alert("Your are child");
} else if (userAge < 18){
     alert("You are teenager")
 }else {
     alert("You are adult")
}

//
// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення, скільки днів у цьому місяці. Результат виводиться в консоль.
//Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
//(Використайте switch case)

let month = prompt("Який зараз місяць?");

switch (month){
    case "січень":
        console.log("31 днів в місяці");
        break;
    case "лютий":
        console.log("28 днів в місяці");
        break;
    case "березень":
        console.log("31 днів в місяці");
        break;
    case "квітень":
        console.log("30 днів в місяці");
        break;
    case "травень":
        console.log("31 днів в місяці");
        break;
    case "червень":
        console.log("30 днів в місяці");
        break;
    case "липень":
        console.log("31 днів в місяці");
        break;
    case "серпень":
        console.log("31 днів в місяці");
        break;
    case "вересень":
        console.log("30 днів в місяці");
        break;
    case "жовтень":
        console.log("31 днів в місяці");
        break;
    case "листопад":
        console.log("30 днів в місяці");
        break;
    case "грудень":
        console.log("31 днів в місяці");
        break;
}