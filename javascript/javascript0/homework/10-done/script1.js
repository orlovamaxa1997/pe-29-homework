// Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
const foot = document.getElementById("footer");

const newA = document.createElement("a")

newA.innerText = ("Learn More");
newA.setAttribute("href", "#");

foot.after(newA);