// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

const newSelect = document.createElement("select");
newSelect.setAttribute("id", "rating");

const main = document.querySelector("#container");
const sect = document.querySelector("#features");

main.insertBefore(newSelect, sect);

//     Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//     Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//     Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//     Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

for (let i = 1; i < 5; i++) {
    const newOption = document.createElement("option");
    newOption.setAttribute("value", i.toString());
    // if (i === 1){
    //     newOption.innerText = `${i} Star`
    // } else {
    //     newOption.innerText = `${i} Stars`
    // }
   // newOption.innerText = i === 1 ? `${i} Star` : `${i} Stars`
    newOption.innerText = `${i} Star${i !== 1 ? "s" :''}`
    main.insertBefore(newOption, sect);
    newSelect.add(newOption);
}