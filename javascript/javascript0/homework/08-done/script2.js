// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//
//     Відфільтрований масив виведіть в консоль.

const userArray =  [
    {
        name: "Іван",
        age: 25,
        sex: "чоловіча"
    },
    {
        name: "Ксенія",
        age: 34,
        sex: "жіноча"
    },
    {
        name: "Степан",
        age: 13,
        sex: "чоловіча"
    }
]

// const userMan = userArray.filter(function (user) {
//     return user.sex === "чоловіча";
// })

const userMan = userArray.filter(function ({sex}) {
    return sex === "чоловіча";
})

console.log(userMan);
