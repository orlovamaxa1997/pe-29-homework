// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

const wordsArray = ["travel", "hello", "eat", "ski", "lift", "ia"]
let count = 0;

for (let i = 0; i < wordsArray.length; i++ ){
    if (wordsArray[i].length > 3) {
        count++;
    }
}

console.log(count);