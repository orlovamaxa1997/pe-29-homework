// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
//
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.
//
const pew = "ротатор";
const verifyPalindrome = (text) => {
    let isPalindrome = true;

    for(let i = 0; i < text.length; i++){
        if (text[i] !== text[text.length -1 -i]) isPalindrome = false;
    }

    return isPalindrome;
}

console.log(verifyPalindrome(pew));

//
//
