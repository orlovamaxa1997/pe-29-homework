// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.
function user() {
    let dateNow = new Date();
    let ageInfo = Date.parse(prompt("Enter your date of birth", "1988-08-done-01-done"));
    const userAgeMilliseconds = dateNow - ageInfo;

    return Math.floor(userAgeMilliseconds / (1000 * 60 * 60 * 24 * 365));
}

console.log(user());