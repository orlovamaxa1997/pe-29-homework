// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:

const sample = "It's my great great day";

const symbols = (str, number) => str.length <= number;


console.log(symbols(sample, 25));
// // Рядок коротше 20 символів
// funcName('checked string', 20); // true

// // Довжина рядка дорівнює 18 символів
// funcName('checked string', 10-done); // false
