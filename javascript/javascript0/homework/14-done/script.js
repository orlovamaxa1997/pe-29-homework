// // Практичне завдання:
// //  Реалізувати можливість зміни колірної теми користувача.
// //
// // Технічні вимоги:
// // - Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
// //
// // - Додати на макеті кнопку "Змінити тему".
// // - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// // - Вибрана тема повинна зберігатися після перезавантаження сторінки.
//
// //
// //     Примітки:
// // - при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
// // - зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.


const buttons = document.querySelectorAll(".btn");
document.addEventListener("keydown", handler);


function handler(e) {
    let activeTheme = localStorage.getItem('theme');

    for (let i = 0; i < buttons.length; i++) {
        if ("Key" + buttons[i].textContent === e.code || buttons[i].textContent === e.key) {
            buttons[i].style.backgroundColor = activeTheme === 'dark' ? '#768fe1' : 'blue';
        } else buttons[i].style.backgroundColor = activeTheme === 'dark' ? 'white': 'black';
    }
}

function changeButtonColorDependsOnTheme(checkedButtonTheme) {
    for (let i = 0; i < buttons.length; i++) {
        if (buttons[i].style.backgroundColor === 'black'){
            buttons[i].style.backgroundColor = 'white';
        }
        else if (buttons[i].style.backgroundColor === 'white'){
            buttons[i].style.backgroundColor = 'black';
        }
        else if (buttons[i].style.backgroundColor === 'blue'){
            buttons[i].style.backgroundColor = '#768fe1';
        }
        else if (buttons[i].style.backgroundColor === 'rgb(118, 143, 225)'){
            buttons[i].style.backgroundColor = 'blue';
        }
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    init()
})
function init() {
    document.documentElement.setAttribute("theme", localStorage.getItem('theme'));
}

const changeBtn = document.querySelector("#change-theme");
changeBtn.addEventListener("click", function() {
    if(document.documentElement.getAttribute("theme") === 'dark'){
        document.documentElement.setAttribute("theme", "light");
        localStorage.setItem('theme', 'light');
        changeButtonColorDependsOnTheme('blue');
    }
    else{
        document.documentElement.setAttribute("theme", "dark");
        localStorage.setItem('theme', 'dark');
        changeButtonColorDependsOnTheme('rgb(118, 143, 225)');
    }
});
