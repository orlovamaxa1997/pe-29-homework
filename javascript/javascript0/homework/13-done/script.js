// Реалізувати перемикання вкладок (таби) на чистому Javascript.
//
//     Технічні вимоги:
// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
//
//
//     Умови:
// - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
//
const tabIdHelper = (title) => `${title}__tab`;
const tabContentIdHelper = (title) => `${title}__content`;

let activeTab = document.getElementById(tabIdHelper("Akali"));
let activeContent = document.getElementById(tabContentIdHelper("Akali"));

addEventListener("click", function(event) {
    if (!event.target.classList.contains("tabs-title")) return;

    // handle change tab
    const clickedTab = document.getElementById(event.target.id);
    activeTab.classList.remove("active");
    clickedTab.classList.add("active");

    activeTab = clickedTab;

    // handle change content
    const clickedContent = document.getElementById(tabContentIdHelper(event.target.innerText))
    activeContent.classList.remove("tabs-content__active");
    clickedContent.classList.add("tabs-content__active");

    activeContent = clickedContent;
});

