// Реалізувати функцію підсвічування клавіш.
//
// Технічні вимоги:
// - У файлі index2.html лежить розмітка для кнопок.
// - Кожна кнопка містить назву клавіші на клавіатурі
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір.
// При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.
// Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
// Але якщо при натискані на кнопку її не існує в розмітці, то попередня активна кнопка повина стати неактивною.
//

const generateButtonId = (key) => `btn__${key}`;
let lastEl ;

addEventListener("keydown", (event) =>{
    // console.log(event.key)
    const btn = document.getElementById(generateButtonId(event.key));
    if (!btn) return;
    if (lastEl){
        lastEl.classList.remove("btn-color");
    }
    btn.classList.add("btn-color");
    lastEl = btn;
    console.log(lastEl)
    // console.log(btn);
    // console.log(generateButtonId(event.key));

})