// 1. Створіть змінну "username" і присвойте їй ваше ім'я. Створіть змінну "password" і присвойте їй пароль (наприклад, "secret").
// Далі ми імітуємо введеня паролю користувачем.
// Отримайте від користувача значення його паролю і перевірте, чи співпадає воно зі значенням в змінній "password". Виведіть результат порівнння в консоль.

const userName = "Maria";
let password = "secret";

let enterPassword = prompt("Writing your password");

if (enterPassword === password){
    console.log("Great")
} else {
    console.log("Error")
}

//
// 2. Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y та запишіть присвойте їй значення 3.
//Виведіть результати додавання, віднімання, множення та ділення змінних x та y у вікні alert.

const x = 5;
const y = 3;

// first option
// alert(x + y);
// alert(x - y);
// alert(x * y);
// alert(x / y);

// second option
alert(`${x + y}  ${x - y}  ${x * y} ${x / y}`);


