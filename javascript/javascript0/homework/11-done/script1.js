// 1. Додати новий абзац по кліку на кнопку:
    // По кліку на кнопку <Button id="btn-click">Click Me</Button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const button = document.getElementById("btn-click");
const section = document.getElementById("content")

button.addEventListener("click" ,() => {
    const el = document.createElement("p");
    el.innerText = "New Paragraph";
    section.append(el)
})

