// 2. Додати новий елемент форми із атрибутами:
//Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const formElement = document.getElementById("form");
const buttonElement = document.getElementById("btn-input-create");
const footerElement = document.getElementById("footer");

formElement.appendChild(buttonElement);
buttonElement.addEventListener("click", ()=> {
    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "market");
    input.setAttribute("name", "phone");
    formElement.append(input)

})