import {DATA} from "./index.js";
import { activeBtns } from "./sort.js";
import { addModalWindow } from "./popup.js";
import { mainLoader } from "./loader.js"
import { filterCards } from "./filter.js";

const cardsContainer = document.querySelector('.trainers-cards__container');
const cardTemplate = document.getElementById('trainer-card');

DATA.forEach((el) => {
    const clone = document.importNode(cardTemplate.content, true);

    clone.querySelector('.trainer__img').src = el.photo;
    clone.querySelector('.trainer__name').innerText = el['first name'] + " " + el['last name'];

    cardsContainer.appendChild(clone);
});

function getSortedDataFromLocalStorage() {
    return JSON.parse(localStorage.getItem('sortedData')) || [];
}

export function renderCards(data) {
    cardsContainer.innerHTML = '';

    data.forEach((el) => {
        const clone = document.importNode(cardTemplate.content, true);

        clone.querySelector('.trainer__img').src = el.photo;
        clone.querySelector('.trainer__name').innerText = el['first name'] + " " + el['last name'];

        cardsContainer.appendChild(clone);
    });
    localStorage.setItem('sortedData', JSON.stringify(data));
}

mainLoader();
renderCards(getSortedDataFromLocalStorage());
activeBtns();
addModalWindow(DATA);
filterCards();
