
const popupTemplate = document.getElementById('Modal-template');
const body = document.querySelector('body');

function showModal(data, index) {
    const cloneModal = popupTemplate.content.cloneNode(true);
    cloneModal.querySelector('.modal__img').src = data[index].photo;
    cloneModal.querySelector('.modal__name').innerText = data[index]['first name'] + " " + data[index]['last name'];
    cloneModal.querySelector('.modal__point--experience').innerText = data[index].experience;
    cloneModal.querySelector('.modal__point--specialization').innerText = data[index].specialization;
    cloneModal.querySelector('.modal__text').innerText = data[index].description;

    body.style.overflowY = "hidden";
    const close = cloneModal.querySelector('.modal__close');
    const block = cloneModal.querySelector('.Modal');

    close.addEventListener('click', () => {
        body.style.overflowY = "auto";
        block.style.display = "none";
    });

    document.body.appendChild(cloneModal);
}

export function addModalWindow(data) {
    function getSortedDataFromLocalStorage() {
        return JSON.parse(localStorage.getItem('sortedData')) || [];
    }

    const sortedData = getSortedDataFromLocalStorage();

    const trainers = document.querySelectorAll('.trainer__show-more');
    trainers.forEach((trainer, index) => {
        trainer.dataset.index = index;
        trainer.addEventListener('click', (e) => {
            const index = e.target.dataset.index;
            showModal(sortedData, index);
        });
    });
}

