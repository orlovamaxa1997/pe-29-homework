import {DATA} from "./index.js";
import { addModalWindow } from "./popup.js";
import { renderCards } from "./main.js";



let filterInput = document.querySelectorAll('.filters__label');
let showButton = document.querySelector('.filters__submit');
export const filterArr = [];
export function filterCards(){
    filterInput.forEach((el) => {
        el.addEventListener('change', () => {
            applyFilter();
        });
    });
    showButton.addEventListener('click', (e) => {
        e.preventDefault();
        filterArr.splice(0, filterArr.length);
        console.log(filterArr);
        applyFilter();
    });
}




function applyFilter() {
    let direction;
    let category;
    filterInput.forEach((el) => {
        const input = document.getElementById(el.htmlFor);
        if(input.checked){
            if(!direction){
                direction = el.textContent.trim().toUpperCase();
            }else{
                category = el.textContent.trim().toUpperCase();
            }
        }
    });
    console.log(direction, category);
    if (direction === "ВСІ" && category === "ВСІ") {
        return renderCards(DATA);
    } else {
        const allDirection = direction === "ВСІ";
        const allCategory = category === "ВСІ";

        let filteredData = DATA.filter(item => {
            if (allDirection && !allCategory) {
                return item.category.toUpperCase() === category;
            } else if (!allDirection && allCategory) {
                return item.specialization.toUpperCase() === direction;
            } else if (direction && category) {
                return item.specialization.toUpperCase() === direction && item.category.toUpperCase() === category;
            }
        });

        filterArr.push(...filteredData);

        renderCards(filteredData);
        addModalWindow(filteredData);
    }
}
console.log(filterArr);