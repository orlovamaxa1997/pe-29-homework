const body = document.body;


export function mainLoader(){
    const loader = document.createElement('div');
    const img = document.createElement('img');

    loader.style.cssText = "width: 100%; height: 100%; background: rgba(247, 247, 247, 0.43); position: fixed; top: 0; display: flex; justify-content: center; align-items: center;";



    img.setAttribute('src', "./img/loader.gif");
    body.appendChild(loader);
    loader.appendChild(img);

    window.onload = function(){
        setTimeout( function() {
            loader.style.display = "none";
            document.querySelector('.sorting').hidden = false;
            document.querySelector('.sidebar').hidden = false;
        }, 2000);

    }
}