import { DATA } from "./index.js";
import { addModalWindow } from "./popup.js";
import { renderCards } from "./main.js";
import { filterArr } from "./filter.js";

export function activeBtns() {
    const sortBtn = Array.from(document.querySelectorAll('.sorting__element'));

    const clearActiveClass = (elem, className = "sorting__btn--active") => {
        elem.find(item => item.classList.remove(className));
    }
    const setActiveClass = (elem, index, className = "sorting__btn--active") => {
        elem[index].classList.add(className);
    }
    const sorted = (item, index) => {
        item.addEventListener('click', () => {

            if(filterArr.length > 0){
                const sortType = index;
                const sortedFilterData = sortCards(filterArr, sortType);

                clearActiveClass(sortBtn);
                setActiveClass(sortBtn, index);
                renderCards(sortedFilterData);
                addModalWindow(sortedFilterData);
                console.log("filter")
            }else if (DATA){
                const sortType = index;
                const sortedData = sortCards(DATA, sortType);


                clearActiveClass(sortBtn);
                setActiveClass(sortBtn, index);
                renderCards(sortedData);
                addModalWindow(sortedData);
                console.log("sort")
            }
        })
    }

    sortBtn.forEach(sorted);
}

function sortCards(dataToSort, sortType) {
    let sortedData = [];

    if (sortType === 2) {
        sortedData = dataToSort.slice().sort(function(a, b) {
            if (a['last name'] < b['last name']) return -1;
            return 0;
        });
    } else if (sortType === 3) {
        sortedData = dataToSort.slice().sort((a, b) => parseInt(a.experience) - parseInt(b.experience));
    } else {
        sortedData = dataToSort.slice().sort(() => Math.random() - 0.5);
    }

    return sortedData;
}






