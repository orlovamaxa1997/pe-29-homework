Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.  

Коли парсуємо JSON-дані, може виникнути помилка, якщо дані не є дійсним JSON. try...catch дозволяє безпечно обробити помилки парсингу.

Якщо не впевнені, що об'єкт має певні властивості, можна використовувати try...catch для обробки помилок при доступі до них.