const logInEmail = 'admin@gmail.com'
const logInPass = 'admin'
const logIn = document.querySelector('.btn-log-in')
const bookVisit = document.querySelector('.btn-book-visit')
const modalLogIn = document.querySelector('.Modal-log-in-wrapper')
const formLogIn = document.forms[0]
const visitWrapper = document.querySelector('.Modal-wrapper')
const doctorData = document.querySelector('.doctor-data')
const closeLogIn = document.querySelector('.btn-close-log-in')
let makeVisit;
const contentCards = document.querySelector('section.content')
const contentEmpty = document.querySelector('.content-empty')
const closeMakeVisit = document.querySelector('.btn-close-Modal')
const dropdownItems = document.querySelectorAll('.dropdown .dropdown-item')
let modalInstance; //додала зміну для області видимості
let doctorDataCardiologist = `<div class="modal-heart"><p class="modal-pressure">Звичайний тиск:</p><input type="text"></div><div class="modal-bmi"><p class="modal-user-bmi">ІМТ:</p><input type="text"></div><div class="modal-diseases"><p class="modal-heart-diseases">Перенесені захворювання серцево-судинної системи:</p><input type="text"></div><div class="modal-age"><p class="modal-user-age">Вік:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Створити візит</button>`
let doctorDataDentist = `<div class="modal-last"><p class="modal-last-visit">Дата останнього відвідування:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Створити візит</button>`
let doctorDataTherapist = `<div class="modal-age"><p class="modal-age">Вік:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Створити візит</button>`
let modalEdit = document.querySelector('.Modal-edit-wrapper')

let editDoctorDataCardiologist = `<div class="modal-heart"><p class="modal-pressure">Звичайний тиск:</p><input type="text"></div><div class="modal-bmi"><p class="modal-user-bmi">ІМТ:</p><input type="text"></div><div class="modal-diseases"><p class="modal-heart-diseases">Перенесені захворювання серцево-судинної системи:</p><input type="text"></div><div class="modal-age"><p class="modal-user-age">Вік:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Зберегти зміни</button>`
let editDoctorDataDentist = `<div class="modal-last"><p class="modal-last-visit">Дата останнього відвідування:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Зберегти зміни</button>`
let editDoctorDataTherapist = `<div class="modal-age"><p class="modal-age">Вік:</p><input type="text"></div><button class="btn btn-primary btn-send-visit">Зберегти зміни</button>`
let editDoctorData = modalEdit.querySelector('.doctor-data')


// DROPDOWN FUNCTIONAL

dropdownItems.forEach(item => {
    item.addEventListener('click', (event) => {
        event.target.parentElement.parentElement.parentElement.children[0].innerText = item.textContent
    })
})


// LOG-IN FORM

logIn.addEventListener('click', () => {
    modalLogIn.style.display = 'flex';
});

closeLogIn.addEventListener('click', () => {
    modalLogIn.style.display = 'none'
})

modalLogIn.addEventListener('click', (event) => {
    if (event.target == modalLogIn) {
        modalLogIn.style.display = 'none'
    }
})


formLogIn.addEventListener('submit', async (event) => {
    event.preventDefault();
    const email = formLogIn.elements.email.value;
    const password = formLogIn.elements.password.value;
    modalInstance = new Modal(email, password);
    modalInstance.validate();
    // Тут потрібно буде викликати ще один метод який я описав у самому классі
    await fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'GET',
        headers: {
            "Authorization": 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
        }
    })
        .then(response => response.json())
        .then(response => response.forEach(function (card) {
            switch (card.doctor) {
                case 'Dentist':
                    card = new VisitDentist(card.name, card.title, card.text, card.urgency, card.statusCard, card.lastVisit, card.id)
                    card.render()
                    break;
                case 'Therapist':
                    card = new VisitTherapist(card.name, card.title, card.text, card.urgency, card.statusCard, card.age, card.id)
                    card.render()
                    break;
                case 'Cardiologist':
                    card = new VisitCardiologist(card.name, card.title, card.text, card.urgency, card.statusCard, card.pressure, card.bmi, card.heartDiseases, card.age, card.id)
                    card.render()
                    break;
            }
        }))
});

// BOOK VISIT FORM

bookVisit.addEventListener('click', () => {
    visitWrapper.style.display = 'flex';
    visitWrapper.querySelectorAll('input').forEach(input => input.value = '')
    doctorData.innerHTML = ''
});


closeMakeVisit.addEventListener('click', () => {
    visitWrapper.style.display = 'none'
    visitWrapper.querySelector('.Modal-doctor').style.display = 'inline-flex'
})

visitWrapper.addEventListener('click', (event) => {
    if (event.target == visitWrapper) {
        visitWrapper.style.display = 'none'
    }
})

// CLASS FOR THE LOG-IN WINDOW
class Modal {
    constructor(email, password) {
        this.email = email
        this.password = password
        this.validated = false
        this.idCard = null;
        this.visit = null;
    }

    validate() {
        if (this.email == logInEmail && this.password == logInPass) {
            alert('Данні вірні, вітаю на сайті admin')
            this.validated = true
            modalLogIn.style.display = 'none'
            logIn.style.display = 'none'
            bookVisit.style.display = 'flex'
        } else {
            alert('Данні не вірні, спробуйте заново')
            formLogIn.elements.password.value = ''
            formLogIn.elements.password.style.border = '2px solid red'
            formLogIn.elements.email.value = ''
            formLogIn.elements.email.style.border = '2px solid red'
        }
    }

    async postCardsRequest(cardData) {
        const URL = "https://ajax.test-danit.com/api/v2/cards";
        const token = '739a7375-e4de-4e2c-ab8e-d4db7dce77ec'; //Токен

        try {
            const response = await fetch(URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(cardData)
            });

            const data = await response.json();
            let idCard = data.id;
            cardData.id = data.id;
            return idCard;

        } catch (error) {
            console.error("Error:", error.message);
        }
    }

    async getALLCards() {
        const token = '739a7375-e4de-4e2c-ab8e-d4db7dce77ec'; // ваш токен

        const url = 'https://ajax.test-danit.com/api/v2/cards';

        try {
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            });

            if (!response.ok) {
                throw new Error('Помилка отримання карточок');
            }

            const cards = await response.json();
            return cards;

        } catch (error) {
            console.error('Помилка при отриманні карточок:', error.message);
        }
    }

    async deleteCardRequest(idCard) {
        try {
            const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${idCard}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
                }
            });


            if (response.ok) {
                console.log('Карточка успішно видалена.');
            } else {
                console.error('Помилка видалення карточки:', response.statusText);
            }
        } catch (error) {
            console.error('Помилка DELETE запиту:', error.message);
        }
    }

    async showCardsVisit(visit, selectedDoctor) {
        try {
            let id = await modalInstance.postCardsRequest(visit);

            const url = `https://ajax.test-danit.com/api/v2/cards/${id}`;
            const token = '739a7375-e4de-4e2c-ab8e-d4db7dce77ec';

            try {
                const response = await fetch(url, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                });

                const cardData = await response.json();

                if (cardData) {

                    let cardVisit;
                    switch (selectedDoctor) {
                        case 'Кардіолог':
                            cardVisit = new VisitCardiologist(visit.name, visit.title, visit.text, visit.urgency, visit.statusCard, visit.pressure, visit.bmi, visit.heartDiseases, visit.age, visit.id);
                            break;
                        case 'Стоматолог':
                            cardVisit = new VisitDentist(visit.name, visit.title, visit.text, visit.urgency, visit.statusCard, visit.lastVisit, visit.id);
                            break;
                        case 'Терапевт':
                            cardVisit = new VisitTherapist(visit.name, visit.title, visit.text, visit.urgency, visit.statusCard, visit.age, visit.id);
                            break;
                        default:
                            console.error('Невідомий тип візиту');
                            return;
                    }
                    cardVisit.render();
                    visitWrapper.style.display = 'none';
                    contentEmpty.style.display = 'none';
                } else {
                    console.error('Помилка: Отримані дані карточки недоступні.');
                }
            } catch (error) {
                console.error("Помилка GET запроса :", error.message);
            }
        } catch (error) {
            console.error('Помилка при відображенні карточки візиту:', error.message);
        }
    }


    async deleteCardHandler(close) {
        const cardElement = close.closest('.card');
        const idCard = cardElement.dataset.id;

        if (idCard) {
            try {
                await modalInstance.deleteCardRequest(idCard);
                cardElement.remove();
                console.log(`Карточка успішно видалена.${idCard}`);
            } catch (error) {
                console.error('Помилка видалення карточки:', error.message);
            }
        } else {
            console.error('Помилка: Немає ідентифікатора карточки.');
        }
    }

    async editCards(id, target) {
        try {
            let cardForEdit = target.parentElement.parentElement.parentElement.parentElement
            let editCardId = id
            modalEdit.style.display = 'flex';
            let card;
            await fetch(`https://ajax.test-danit.com/api/v2/cards/${editCardId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
                }
            })
                .then(response => response.json()).then(response => {
                    card = response;
                });
            modalEdit.querySelector('.Modal-info input').value = card.name
            modalEdit.querySelector('.Modal-title input').value = card.title
            modalEdit.querySelector('.Modal-text input').value = card.text
            modalEdit.querySelector('.Modal-urgency Button').innerText = card.urgency
            modalEdit.querySelector('.Modal-status Button').innerText = card.statusCard
            let editVisit, name, title, text, urgency,statusCard, age, pressure, bmi, diseases, lastVisit, visit;
            let closeEdition = modalEdit.querySelector('.btn-close-Modal')
            closeEdition.addEventListener('click',()=>{
                modalEdit.style.display = 'none'
            })
            modalEdit.addEventListener('click',(event)=>{
                if(event.target == modalEdit){
                    modalEdit.style.display = 'none'
                }
            })
            switch (card.doctor) {
                case 'Cardiologist':
                    editDoctorData.innerHTML = editDoctorDataCardiologist
                    modalEdit.querySelector('.Modal-age input').value = card.age
                    modalEdit.querySelector('.Modal-heart input').value = card.pressure
                    modalEdit.querySelector('.Modal-bmi input').value = card.bmi
                    modalEdit.querySelector('.Modal-diseases input').value = card.heartDiseases
                    editVisit = modalEdit.querySelector('.btn-send-visit')
                    editVisit.addEventListener('click', async () => {
                        name = modalEdit.querySelector('.Modal-info input').value
                        title = modalEdit.querySelector('.Modal-title input').value
                        text = modalEdit.querySelector('.Modal-text input').value
                        urgency = modalEdit.querySelector('.Modal-urgency Button').innerText
                        statusCard = modalEdit.querySelector('.Modal-status Button').innerText
                        age = modalEdit.querySelector('.Modal-age input').value
                        pressure = modalEdit.querySelector('.Modal-heart input').value
                        bmi = modalEdit.querySelector('.Modal-bmi input').value
                        diseases = modalEdit.querySelector('.Modal-diseases input').value
                        cardForEdit.querySelector('.card-body-name').innerText = name
                        cardForEdit.querySelector('.card-title-text').innerText = title
                        cardForEdit.querySelector('.card-text').innerText = text
                        cardForEdit.querySelector('.card-title-urgency').innerText = "Терміновість: " + urgency
                        cardForEdit.querySelector('.card-body-status-text').innerText = 'Статус: ' + statusCard
                        cardForEdit.querySelector('.card-user-age').innerText = age
                        cardForEdit.querySelector('.card-user-pressure').innerText = pressure
                        cardForEdit.querySelector('.card-user-bmi').innerText = bmi
                        cardForEdit.querySelector('.card-user-diseases').innerText = diseases
                        modalEdit.style.display = 'none'
                        visit = new VisitCardiologist(name, title, text, urgency, statusCard, pressure, bmi, diseases, age, id)
                        await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
                            },
                            body: JSON.stringify(visit)
                        })
                        console.log('Картку успішно відредаговано');
                    })

                    break;
                case 'Dentist':
                    editDoctorData.innerHTML = editDoctorDataDentist
                    modalEdit.querySelector('.Modal-last input').value = card.lastVisit
                    editVisit = modalEdit.querySelector('.btn-send-visit')
                    editVisit.addEventListener('click', async () => {
                        name = modalEdit.querySelector('.Modal-info input').value
                        title = modalEdit.querySelector('.Modal-title input').value
                        text = modalEdit.querySelector('.Modal-text input').value
                        statusCard = modalEdit.querySelector('.Modal-status Button').innerText
                        urgency = modalEdit.querySelector('.Modal-urgency Button').innerText
                        lastVisit = modalEdit.querySelector('.Modal-last input').value
                        cardForEdit.querySelector('.card-body-name').innerText = name
                        cardForEdit.querySelector('.card-title-text').innerText = title
                        cardForEdit.querySelector('.card-text').innerText = text
                        cardForEdit.querySelector('.card-title-urgency').innerText = "Терміновість: " + urgency
                        cardForEdit.querySelector('.card-body-status-text').innerText = 'Статус: ' + statusCard
                        cardForEdit.querySelector('.card-user-last-visit').innerText = "Останній візит: " + lastVisit
                        modalEdit.style.display = 'none'
                        visit = new VisitDentist(name, title, text, urgency, statusCard, lastVisit, id)
                        await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
                            },
                            body: JSON.stringify(visit)
                        })
                        console.log('Картку успішно відредаговано');
                    })
                    break;
                case 'Therapist':
                    editDoctorData.innerHTML = editDoctorDataTherapist
                    modalEdit.querySelector('.Modal-age input').value = card.age
                    editVisit = modalEdit.querySelector('.btn-send-visit')
                    editVisit.addEventListener('click', async () => {
                        name = modalEdit.querySelector('.Modal-info input').value
                        title = modalEdit.querySelector('.Modal-title input').value
                        text = modalEdit.querySelector('.Modal-text input').value
                        statusCard = modalEdit.querySelector('.Modal-status Button').innerText
                        urgency = modalEdit.querySelector('.Modal-urgency Button').innerText
                        age = modalEdit.querySelector('.Modal-age input').value
                        cardForEdit.querySelector('.card-body-name').innerText = name
                        cardForEdit.querySelector('.card-title-text').innerText = title
                        cardForEdit.querySelector('.card-text').innerText = text
                        cardForEdit.querySelector('.card-title-urgency').innerText = "Терміновість: " + urgency
                        cardForEdit.querySelector('.card-body-status-text').innerText = 'Статус: ' + statusCard
                        cardForEdit.querySelector('.card-user-age').innerText = "Вік" + age
                        modalEdit.style.display = 'none'
                        visit = new VisitTherapist(name, title, text, urgency,statusCard , age, id)
                        await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer 739a7375-e4de-4e2c-ab8e-d4db7dce77ec'
                            },
                            body: JSON.stringify(visit)
                        })
                        console.log('Картку успішно відредаговано');
                    })
                    break;
                default:
                    break;
            }
        } catch (error) {
            console.error("Сталася помилка при редагуванні картки:", error.message);
        }
    }

}

// MAIN CLASS FOR VISIT CARDS

class Visit {
    constructor(name, title, text, urgency, statusCard) {
        this.name = name;
        this.text = text;
        this.title = title;
        this.urgency = urgency;
        this.statusCard = statusCard;
    }
}

 // EXTENSION OF CLASS 'Visit', ADDING A FULL HTML CARDS

class VisitDentist extends Visit {
    constructor(name, title, text, urgency, statusCard, lastVisit, id) {
        super(name, title, text, urgency, statusCard);
        this.lastVisit = lastVisit;
        this.id = id;
        this.doctor = 'Dentist'
    }

    render() {
        let div = document.createElement('div')
        div.classList.add('card')
        div.dataset.id = this.id; //додала дата атрибут до карточки


        div.innerHTML = `
        <div class="card-body">
            <div class="card-body-info">
                <p class="card-body-name">Імя клієнта: ${this.name}</p>
                <button class="btn btn-close"></button>
            </div>
            <div class="card-body-doctor">
                <p class="card-body-doctor-name">Лікар: Стоматолог</p>
                <button class="card-body-edit-button"><img src="img/icons8-edit-text-file-50.png" alt="edit_button"></button>
            </div>
            <div class="card-title">
                <p class="card-title-text">Мета візиту: ${this.title}</p>
                <p class="card-title-urgency">Терміновість: ${this.urgency}</p>
            </div>
            <p class="card-text">Опис візиту: ${this.text}</p>
            <p class="card-user-last-visit">Останній візит до стамтолога: ${this.lastVisit}</p>
            </div>
            <div class="card-body-status">
        <p class="card-body-status-text">Статус: ${this.statusCard}</p>
        </div>
            <a class="btn btn-outline-primary card-btn-show-more">Показати більше</a>
            <a class="btn btn-outline-secondary card-btn-hide">Згорнути</a>
        `
        const cardShowMore = div.querySelector('.card-btn-show-more')
        const cardHideContent = div.querySelector('.card-btn-hide')
        showOrHide(cardShowMore, cardHideContent, div)
        contentEmpty.style.display = 'none'
        contentCards.append(div)


        const close = div.querySelector('.btn-close')
        //ВИДАЛЕННЯ КАРТОЧКИ
        close.addEventListener("click", async () => {
            await modalInstance.deleteCardHandler(close);
        });


        const editBtn = div.querySelector('.card-body-edit-Button')
        editBtn.addEventListener("click", async (event) => {
            await modalInstance.editCards(this.id, event.target)

        });
    }
}

class VisitCardiologist extends Visit {
    constructor(name, title, text, urgency, statusCard, pressure, bmi, heartDiseases, age, id) {
        super(name, title, text, urgency, statusCard);
        this.pressure = pressure;
        this.bmi = bmi;
        this.heartDiseases = heartDiseases;
        this.age = age;
        this.id = id;
        this.doctor = 'Cardiologist'
    }

    render() {
        let div = document.createElement('div')
        div.classList.add('card')
        div.dataset.id = this.id;


        div.innerHTML = `
        <div class="card-body">

            <div class="card-body-info">
                <p class="card-body-name">Імя клієнта: ${this.name}</p>
                <button class="btn btn-close"></button>
            </div>
            <div class="card-body-doctor">
                <p class="card-body-doctor-name">Лікар: Кардіолог</p>
                <button class="card-body-edit-button"><img src="img/icons8-edit-text-file-50.png" alt="edit_button"></button>
            </div>
            <div class="card-title">
                <p class="card-title-text">Мета візиту: ${this.title}</p>
                <p class="card-title-urgency">Tерміновість: ${this.urgency}</p>
            </div>
            <p class="card-text">Опис візиту: ${this.text}</p>
            <div class="card-user-heart">
                    <p class="card-user-pressure">Пульс: ${this.pressure}</p>
                    <p class="card-user-bmi">ІМТ: ${this.bmi}</p>
            </div>
            <div class="card-user-additional">
                <p class="card-user-diseases">Перенесенні захворювання: ${this.heartDiseases}</p>
                <p class="card-user-age">Вік: ${this.age}</p>
            </div>
        </div>
        <div class="card-body-status">
        <p class="card-body-status-text">Статус: ${this.statusCard}</p>
        </div>
        <a class="btn btn-outline-primary card-btn-show-more">Показати більше</a>
        <a class="btn btn-outline-secondary card-btn-hide">Згорнути</a>
        `
        const cardShowMore = div.querySelector('.card-btn-show-more')
        const cardHideContent = div.querySelector('.card-btn-hide')
        showOrHide(cardShowMore, cardHideContent, div)
        contentEmpty.style.display = 'none'
        contentCards.append(div)

        const close = div.querySelector('.btn-close')

        close.addEventListener("click", async () => {
            await modalInstance.deleteCardHandler(close);
        });

        const editBtn = div.querySelector('.card-body-edit-Button')
        editBtn.addEventListener("click", async (event) => {
            await modalInstance.editCards(this.id, event.target)
        })
    }
}

class VisitTherapist extends Visit {
    constructor(name, title, text, urgency, statusCard, age, id) {
        super(name, title, text, urgency, statusCard);
        this.age = age;
        this.id = id;
        this.doctor = 'Therapist'
    }

    render() {
        let div = document.createElement('div')
        div.classList.add('card')
        div.dataset.id = this.id;

        div.innerHTML = `
        <div class="card-body">

            <div class="card-body-info">
                <p class="card-body-name">Імя клієнта: ${this.name}</p>
                <button class="btn btn-close"></button>
            </div>
            <div class="card-body-doctor">
                <p class="card-body-doctor-name">Лікар: Терапевт</p>
                <button class="card-body-edit-button"><img src="img/icons8-edit-text-file-50.png" alt="edit_button"></button>
            </div>
            <div class="card-title">
                <p class="card-title-text">Мета візиту: ${this.title}</p>
                <p class="card-title-urgency">Терміновість: ${this.urgency}</p>
            </div>
            <p class="card-text">Опис візиту: ${this.text}</p>
            <p class="card-user-age">Вік: ${this.age}</p>
            </div>
            <div class="card-body-status">
        <p class="card-body-status-text">Статус: ${this.statusCard}</p>
        </div>
            <a class="btn btn-outline-primary card-btn-show-more">Показати більше</a>
            <a class="btn btn-outline-secondary card-btn-hide">Згорнути</a>
        `
        const cardShowMore = div.querySelector('.card-btn-show-more')
        const cardHideContent = div.querySelector('.card-btn-hide')
        showOrHide(cardShowMore, cardHideContent, div)
        contentEmpty.style.display = 'none'
        contentCards.append(div)

        const close = div.querySelector('.btn-close')
        close.addEventListener("click", async () => {
            await modalInstance.deleteCardHandler(close);
        });

        const editBtn = div.querySelector('.card-body-edit-Button')

        editBtn.addEventListener("click", async (event) => {
            await modalInstance.editCards(this.id, event.target)
        })
    }
}

 // EVENT LISTENERS FOR MAKE A VISIT FORM. Adding especial inputs, depends on doctor u choose

const doctorDropdownItems = document.querySelectorAll('.dropdown-menu .dropdown-item');
doctorDropdownItems.forEach(item => {
    item.addEventListener('click', () => {
        const selectedDoctor = item.textContent.trim();

        switch (selectedDoctor) {
            case 'Кардіолог':
                doctorData.innerHTML = doctorDataCardiologist
                makeVisit = document.querySelector('.btn-send-visit')
                makeVisit.addEventListener('click', async () => {
                    let name = document.querySelector('.Modal-info input').value
                    let title = document.querySelector('.Modal-title input').value
                    let text = document.querySelector('.Modal-text input').value
                    let urgency = document.querySelector('.Modal-urgency Button').innerText
                    let age = document.querySelector('.Modal-age input').value
                    let pressure = document.querySelector('.Modal-heart input').value
                    let bmi = document.querySelector('.Modal-bmi input').value
                    let heartDiseases = document.querySelector('.Modal-diseases input').value
                    let statusCard = document.querySelector('.Modal-status Button').innerText;
                    let visit = new VisitCardiologist(name, title, text, urgency, statusCard, pressure, bmi, heartDiseases, age);
                    await modalInstance.showCardsVisit(visit, selectedDoctor);
                })

                break;
            case 'Стоматолог':
                doctorData.innerHTML = doctorDataDentist
                makeVisit = document.querySelector('.btn-send-visit')
                makeVisit.addEventListener('click', async () => {
                    let name = document.querySelector('.Modal-info input').value
                    let title = document.querySelector('.Modal-title input').value
                    let text = document.querySelector('.Modal-text input').value
                    let urgency = document.querySelector('.Modal-urgency Button').innerText
                    let lastVisit = document.querySelector('.Modal-last input').value
                    let statusCard = document.querySelector('.Modal-status Button').innerText;
                    let visit = new VisitDentist(name, title, text, urgency, statusCard, lastVisit);
                    await modalInstance.showCardsVisit(visit, selectedDoctor);//додано для відображення карток
                })
                break;
            case 'Терапевт':
                doctorData.innerHTML = doctorDataTherapist
                makeVisit = document.querySelector('.btn-send-visit')
                makeVisit.addEventListener('click', async () => {
                    let name = document.querySelector('.Modal-info input').value
                    let title = document.querySelector('.Modal-title input').value
                    let text = document.querySelector('.Modal-text input').value
                    let urgency = document.querySelector('.Modal-urgency Button').innerText
                    let age = document.querySelector('.Modal-age input').value
                    let statusCard = document.querySelector('.Modal-status Button').innerText;
                    let visit = new VisitTherapist(name, title, text, urgency, statusCard, age);
                    await modalInstance.showCardsVisit(visit, selectedDoctor);

                })
                break;
            default:
                break;
        }
    });
});

// Repeating moves function

function showOrHide(showMore, hide, div) {
    showMore.addEventListener('click', () => {
        div.querySelector('.card-body').style.height = 'auto'
        showMore.style.display = 'none'
        hide.style.display = 'inline-block'
    })
    hide.addEventListener('click', () => {
        div.querySelector('.card-body').style.height = '120px'
        showMore.style.display = 'inline-block'
        hide.style.display = 'none'
    })
}


//Сортування
const form = document.querySelector('.d-flex.w-100.justify-content-end');
const inputSearch = document.querySelector('.form-control');

async function filterCards() {
    const statusItems = form.querySelectorAll('.dropdown-toggle');
    const searchText = inputSearch.value.trim().toLowerCase();

    const statusText = statusItems[0].textContent.trim();
    const priorityText = statusItems[1].textContent.trim();
    let allCards = await modalInstance.getALLCards();


    let filteredCards;

    if (statusText === 'Статус' && priorityText === 'Терміновість' && searchText === '') {
        filteredCards = allCards;
    } else {

        if (statusText !== 'Статус') {
            allCards = allCards.filter(card => card.statusCard === statusText);
        }


        if (priorityText !== 'Терміновість') {
            allCards = allCards.filter(card => card.urgency === priorityText);
        }

        if (searchText !== '') {
            allCards = allCards.filter(card => {
                if (!card) return false;
                const titleMatch = card.title && card.title.toLowerCase().includes(searchText);
                const descriptionMatch = card.text && card.text.toLowerCase().includes(searchText);
                return titleMatch || descriptionMatch;
            });
        }

        filteredCards = allCards;
    }

    return filteredCards;
}


function toggleFilterContent(filteredCards) {
    const filterContent = document.querySelector(".filter-content");
    if (filteredCards.length === 0) {
        filterContent.style.display = "block";
    } else {
        filterContent.style.display = "none";
    }
}

function updateCardDisplay(filteredCards) {
    const allRenderedCards = document.querySelectorAll('.card');
    const filteredCardIds = filteredCards.map(card => card.id);

    allRenderedCards.forEach(card => {
        const cardId = parseInt(card.dataset.id);
        card.style.display = filteredCardIds.includes(cardId) ? 'block' : 'none';
    });
}

form.addEventListener("submit", async (event) => {
    event.preventDefault();

    const filteredCards = await filterCards();

    toggleFilterContent(filteredCards);
    updateCardDisplay(filteredCards);
});

