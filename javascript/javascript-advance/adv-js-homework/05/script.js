// // Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
//
// //     Технічні вимоги:
// //     При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// //     https://ajax.test-danit.com/api/json/users
// //     https://ajax.test-danit.com/api/json/posts
//
//
// //     Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//
//
//
// //     Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//
//
// // На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//
//
// //     Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
//
//
// //     Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//
//
//
// //     Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
//
// /*


async function fetchData() {
    try {
        const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
        const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');

        const users = await usersResponse.json();
        const posts = await postsResponse.json();

        const cardContainer = document.getElementById('card-container');
        cardContainer.innerHTML = '';


        posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            if (user) {
                const cardProps = {
                    profileImage: 'https://via.placeholder.com/50', // Можна замінити на реальне зображення
                    name: user.name,
                    username: user.username,
                    time: 'just now', // можно додати тут реальний час публікації, якщо доступно
                    text: post.body,
                    comments: '284',
                    retweets: '988',
                    likes: '10,5 тис.'
                };

                const card = new Card(cardProps);
                card.renderCard();
            }
        });
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Клас для створення карток
class Card {
    constructor(props) {
        this.profileImage = props.profileImage;
        this.name = props.name;
        this.username = props.username;
        this.time = props.time;
        this.text = props.text;
        this.comments = props.comments;
        this.retweets = props.retweets;
        this.likes = props.likes;
    }

    renderCard() {
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');

        cardElement.innerHTML = `
            <div class="card-header">
                <img src="${this.profileImage}" alt="Profile Image" class="profile-image">
                <div class="user-info">
                    <strong>${this.name}</strong> 
                    <span class="username">@${this.username} · ${this.time}</span>
                    <div class="user-role">Twitter User</div>
                </div>
            </div>
            <div class="card-body">
                <p>${this.text}</p>
            </div>
            <div class="card-footer">
                <div class="card-footer-item">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14 9h-2v6h6v-2h-4zm6-3H4v14h14v-9h2v11H2V5h18zm-5-4v2h5v5h-2V4h-3V2z"/></svg>
                    <span>${this.comments}</span>
                </div>
                <div class="card-footer-item">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.56 2.66l.12.18a8.85 8.85 0 0 1-.28 9.67c-.09.12-.2.22-.3.33l-2.73 2.73-.13.14-1.78 1.78-6.8-6.8L4 8.73l1.54-1.54 5.66 5.66 1.76-1.76-3.67-3.67c-3.42 2.63-8.5 1.35-10.43-3.18a.45.45 0 0 1-.04-.17L2 3h4.56c.13 0 .25.02.38.04a8.85 8.85 0 0 1 9.62 9.77l-.26-.12 5.02 5.02z"/></svg>
                    <span>${this.retweets}</span>
                </div>
                <div class="card-footer-item">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16 4.51A6.5 6.5 0 0 1 17.5 17a6.35 6.35 0 0 1-6.22-6.5V4h-2V2h4V0h2v4h4v2h-4v4a4.34 4.34 0 0 0 1.5 3.5v-7h-2v5h-1v1h-1V6h-1V4h1z"/></svg>
                    <span>${this.likes}</span>
                </div>
            </div>
        `;

        document.getElementById('card-container').appendChild(cardElement);
    }
}

fetchData();
