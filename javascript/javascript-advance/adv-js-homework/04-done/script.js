// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
//
//     Технічні вимоги:
//     Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

//     Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.

//     Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.


function fetchData(url) {
    return fetch(url).then(response => response.json());
}

function getFilms() {
    fetchData('https://ajax.test-danit.com/api/swapi/films')
        .then(films => {
            const filmsContainer = document.getElementById('films');
            films.forEach(film => {

                const filmElement = document.createElement('div');
                filmElement.classList.add('film');
                filmElement.innerHTML = `
                            <h2>Episode ${film.episodeId}: ${film.name}</h2>
                            <p>${film.openingCrawl}</p>
                            <ul id="characters-${film.id}">Loading characters...</ul> `;
                filmsContainer.appendChild(filmElement);

                getCharacters(film.id, film.characters);
            });
        })
        .catch(error => console.error('Error fetching films:', error));
}

function getCharacters(filmId, charactersUrls) {
    const charactersPromises = charactersUrls.map(url => fetchData(url));
    Promise.all(charactersPromises)
        .then(characters => {
            const charactersList = document.getElementById(`characters-${filmId}`);
            charactersList.innerHTML = ''; // Очищаємо "Loading characters..."
            characters.forEach(character => {
                const characterElement = document.createElement('li');
                characterElement.textContent = character.name;
                charactersList.appendChild(characterElement);
            });
        })
        .catch(error => console.error(`Error fetching characters for film ${filmId}:`, error));
}

getFilms();