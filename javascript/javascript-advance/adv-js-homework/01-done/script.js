// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }


    //     Створіть гетери та сеттери для цих властивостей.
    get name(){
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get age(){
        return this._age;
    }
    set age(value) {
        this._age = value;
    }

    get salary(){
        return this._salary
    }
    set salary(value) {
        this._salary = value
    }

}

//     Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
class  Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
    get salary(){
        return this._salary;
    }

    set salary(value) {
        this._salary = value * 3
    }

}


const prog1 = new Programmer("Nick", 25, 9000, "eng")
const prog2  = new Programmer("Mary", 25, 1000, "ua")

// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

console.log(prog1, prog2)