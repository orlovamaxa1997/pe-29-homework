const getIPBtn = document.querySelector('.get-ip');
const viewIP = document.querySelector('.set-ip');

const continentValue = document.querySelector('.continent__value');
const countryValue = document.querySelector('.country__value');
const regionValue = document.querySelector('.region__value');
const cityValue = document.querySelector('.city__value');
const districtValue = document.querySelector('.district__value');

async function getIP() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const { ip } = await response.json();
        setIP(ip);
        getAddress(ip);
    } catch (error) {
        console.log(error);
    }
}

async function getAddress(ip) {
    try {
        const response = await fetch(
            `http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`
        );
        const data = await response.json();
        setAddress(data);
    } catch (error) {
        console.log(error);
    }
}

function setAddress({ continent, country, regionName, city, district }) {
    continentValue.innerText = continent || 'none';
    countryValue.innerText = country || 'none';
    regionValue.innerText = regionName || 'none';
    cityValue.innerText = city || 'none';
    districtValue.innerText = district || 'none';
}

function setIP(ip) {
    viewIP.innerText = ip;
}

getIPBtn.addEventListener('click', getIP);
